import {Category} from "../core/models/category";
import {NavigationBar, NestedNavigationBar} from "../core/models/navigation-bar";
import {Menu} from "../core/models/menu";

export class DummyData {
  static categories: Category[] = [
    {id: 1, name: 'All Menu'},
    {id: 2, name: 'Chicken'},
    {id: 3, name: 'Beef'},
    {id: 4, name: 'Drink'},
    {id: 5, name: 'Snack'},
  ];

  static navigationBar: NestedNavigationBar[] = [
    {
      navigations: [
        {id: 1, name: 'Home', icon: 'home'},
        {id: 2, name: 'Menu', icon: 'grid_view'},
        {id: 3, name: 'History', icon: 'description'},
        {id: 4, name: 'Promo', icon: 'sell'}]
    },
    {
      navigations: [
        {id: 5, name: 'Partners', icon: 'person'},
        {id: 6, name: 'Settings', icon: 'settings'},
      ]
    },
  ];

  static menuList: Menu[] = [
    {id: 1, name: 'Single Burger', icon: 'single_burger.png', description: 'Single beef burger with extra cheese', price: 4.5, category: this.categories[2]},
    {id: 2, name: 'Double Beef Burger', icon: 'double_beef.png', description: 'Double beef burger with extra cheese', price: 5.5, category: this.categories[2]},
    {id: 3, name: 'Original Burger', icon: 'original_burger.png', description: 'Original burger with extra cheese', price: 5.0, category: this.categories[2]},
    {id: 4, name: 'Big Mac Burger', icon: 'big_mac_burger.png', description: 'Big mac burger with extra cheese', price: 5.8, category: this.categories[2]},
    {id: 5, name: 'French Fries', icon: 'chips.png', description: 'Potatoes fried until crispy and delicious', price: 2.2, category: this.categories[4]},
    {id: 6, name: 'Original Chicken', icon: 'original_chicken.png', description: 'Original Chicken with spicy sauce', price: 4.8, category: this.categories[1]},
    {id: 7, name: 'Bbq Chicken', icon: 'bbq_chicken.png', description: 'Barbeque chicken is fried chicken with bbq sauce', price: 5.2, category: this.categories[1]},
    {id: 8, name: 'Beef Kebab', icon: 'beef_kebab.png', description: 'Kebab with beef and melted cheese', price: 4.2, category: this.categories[2]},
    {id: 9, name: 'Chicken Kebab', icon: 'chicken_kebab.png', description: 'Kebab with chicken and spicy sauce', price: 5.5, category: this.categories[1]},
  ];
}
