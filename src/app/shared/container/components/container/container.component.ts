import {Component, OnInit} from '@angular/core';
import {NestedNavigationBar} from "../../../../core/models/navigation-bar";
import {NavbarService} from "../../../../core/navbar/services/navbar.service";

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})

export class ContainerComponent implements OnInit {

  navigationBar: NestedNavigationBar[] = [];

  constructor(private navbarService: NavbarService) {
  }

  ngOnInit(): void {
    this.navigationBar = this.navbarService.navbarItems();
  }
}
