import {Component, Input} from '@angular/core';
import {NestedNavigationBar} from "../../../../core/models/navigation-bar";

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.scss']
})
export class NavigationBarComponent {
  @Input() navigationBarItems: NestedNavigationBar[] = [];
}
