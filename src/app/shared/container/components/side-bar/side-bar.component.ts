import {Component, OnInit} from '@angular/core';
import {OrderService} from "../../../../core/order/services/order.service";
import {Order} from "../../../../core/models/order";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";
import {Menu} from "../../../../core/models/menu";
import {OrderHelper} from "../../../../core/order/helper/order.helper";

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
@UntilDestroy()
export class SideBarComponent implements OnInit {
  subTotal = 0;
  grandTotal = 0;
  tax = 0;
  order: Order;

  constructor(private orderService: OrderService) {
  }

  ngOnInit(): void {
    this.orderService.orderedMenu$().pipe(
      untilDestroyed(this)
    ).subscribe(order => {
      this.order = order;
      this.subTotal = OrderHelper.countSubTotal(order);
      // PPn 11%
      this.tax = this.subTotal * 0.11;
      this.grandTotal = OrderHelper.countGrandTotalWithTax(this.subTotal);
    })
  }

  increaseOrderQuantity(menu: Menu): void {
    this.orderService.increaseOrderQuantity(menu);
  }

  decreaseOrderQuantity(menu: Menu): void {
    this.orderService.decreaseOrderQuantity(menu);
  }

  removeMenuFromOrder(menu: Menu): void {
    this.orderService.removeMenuFromOrder(menu);
  }
}
