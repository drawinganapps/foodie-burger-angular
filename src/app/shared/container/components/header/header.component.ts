import {Component, OnInit} from '@angular/core';
import {CategoryService} from "../../../../core/category/services/category.service";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";
import {take} from "rxjs";
import {Category} from "../../../../core/models/category";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

@UntilDestroy()
export class HeaderComponent implements OnInit {
  categories: Category[] = [];
  selectedCategory: Category;
  todayDate: string = Date.now().toString();

  constructor(private categoryService: CategoryService) {
  }

  ngOnInit() {
    this.categoryService.getAllCategories().pipe(
      take(1)
    ).subscribe((categories) => {
      this.categories = categories;
    });

    this.categoryService.selectedCategory$.pipe(
      untilDestroyed(this)
    ).subscribe((category) => {
      this.selectedCategory = category;
    });
  }

  onCategoryChange(category: Category): void {
    this.categoryService.setSelectedCategory(category);
  }

}
