import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ContainerComponent} from './components/container/container.component';
import {NavigationBarComponent} from './components/navigation-bar/navigation-bar.component';
import {SideBarComponent} from './components/side-bar/side-bar.component';
import {RouterOutlet} from "@angular/router";
import {MatIconModule} from "@angular/material/icon";
import {HeaderComponent} from './components/header/header.component';
import {MatInputModule} from "@angular/material/input";
import {CategoryModule} from "../../core/category/category.module";
import {NavbarModule} from "../../core/navbar/navbar.module";
import {MatButtonModule} from "@angular/material/button";
import {OrderModule} from "../../core/order/order.module";
import {MatMenuModule} from "@angular/material/menu";


@NgModule({
  declarations: [
    ContainerComponent,
    NavigationBarComponent,
    SideBarComponent,
    HeaderComponent
  ],
  exports: [
    ContainerComponent
  ],
    imports: [
        CommonModule,
        RouterOutlet,
        MatIconModule,
        MatInputModule,
        CategoryModule,
        NavbarModule,
        MatButtonModule,
        OrderModule,
        MatMenuModule
    ]
})
export class ContainerModule {
}
