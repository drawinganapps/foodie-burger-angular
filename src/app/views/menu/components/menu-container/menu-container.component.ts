import {Component, OnInit} from '@angular/core';
import {Menu} from "../../../../core/models/menu";
import {MenuService} from "../../../../core/menu/services/menu.service";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";
import {combineLatest, map} from "rxjs";
import {OrderService} from "../../../../core/order/services/order.service";
import {Order} from "../../../../core/models/order";
import {CategoryService} from "../../../../core/category/services/category.service";

@UntilDestroy()
@Component({
  selector: 'app-menu-container',
  templateUrl: './menu-container.component.html',
  styleUrls: ['./menu-container.component.scss']
})
export class MenuContainerComponent implements OnInit {
  menuList: Menu[] = [];
  declare orderedMenu: Order;

  constructor(private menuService: MenuService, private orderService: OrderService, private categoryService: CategoryService) {
  }

  ngOnInit(): void {
    combineLatest([this.menuService.getAllMenu(), this.categoryService.selectedCategory$]).pipe(
      map(([menus, category]) => {
        if (category.id === 1) {
          return menus as Menu[];
        }
        return menus.filter(menu => menu.category.id === category.id) as Menu[];
      }),
      untilDestroyed(this),
    ).subscribe((menus) => {
      this.menuList = menus;
    });

    this.orderService.orderedMenu$().pipe(
      untilDestroyed(this),
    ).subscribe(order => {
      this.orderedMenu = order;
    });
  }

  addMenuToCart(menu: Menu): void {
    this.orderService.addMenuToOrder(menu, 1);
  }

  isMenuOrdered(menu: Menu): boolean {
    return !!this.orderedMenu && !!this.orderedMenu.orderedMenu.find(mn => mn.menu.id === menu.id);
  }

  protected readonly event = event;
}
