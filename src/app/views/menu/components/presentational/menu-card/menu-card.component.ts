import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Menu} from "../../../../../core/models/menu";

@Component({
  selector: 'app-menu-card',
  templateUrl: './menu-card.component.html',
  styleUrls: ['./menu-card.component.scss']
})
export class MenuCardComponent {
  @Input() menu: Menu;
  @Input() isMenuAdded: boolean = false;
  @Output() addMenuToCart: EventEmitter<Menu> = new EventEmitter();

  constructor() {
  }

  onMenuAddToCart(): void {
    this.addMenuToCart.emit(this.menu);
  }
}
