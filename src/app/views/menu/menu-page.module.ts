import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuContainerComponent } from './components/menu-container/menu-container.component';
import {RouterModule, Routes} from "@angular/router";
import {ContainerModule} from "../../shared/container/container.module";
import {MenuModule} from "../../core/menu/menu.module";
import { MenuCardComponent } from './components/presentational/menu-card/menu-card.component';
import {OrderModule} from "../../core/order/order.module";
import {MatMenuModule} from "@angular/material/menu";

const routes: Routes = [
  {
    path: '',
    component: MenuContainerComponent
  }
];

@NgModule({
  declarations: [
    MenuContainerComponent,
    MenuCardComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        ContainerModule,
        MenuModule,
        OrderModule,
        MatMenuModule
    ]
})
export class MenuPageModule { }
