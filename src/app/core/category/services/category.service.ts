import { Injectable } from '@angular/core';
import {Category} from "../../models/category";
import {BehaviorSubject, Observable, of} from "rxjs";
import {DummyData} from "../../../shared/dummy-data";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private _selectedCategory$ = new BehaviorSubject<Category>(DummyData.categories[0]);

  constructor() { }

  getAllCategories(): Observable<Category[]> {
    return of(DummyData.categories);
  }

  get selectedCategory$(): Observable<Category> {
    return this._selectedCategory$.asObservable();
  }

  setSelectedCategory(category: Category): void {
    this._selectedCategory$.next(category);
  }

}
