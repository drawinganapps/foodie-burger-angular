import {Category} from "./category";

export interface Menu {
  id: number;
  name: string;
  icon: string;
  price: number;
  description: string;
  category: Category;
}
