import {Menu} from "./menu";

export interface Order {
  id?: number;
  orderedMenu: OrderedMenu[]
}

export interface OrderedMenu {
  menu: Menu;
  total: number;
}
