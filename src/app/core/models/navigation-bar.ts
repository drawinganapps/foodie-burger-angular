export interface NavigationBar {
  id: number;
  name: string;
  icon: string;
}

export interface NestedNavigationBar {
  navigations: NavigationBar[];
}
