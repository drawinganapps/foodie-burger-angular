import {Order} from "../../models/order";

export class OrderHelper {
  static countSubTotal(orderedMenu: Order) {
    let countTotal = 0.0;
    for (let element of orderedMenu.orderedMenu) {
      const total = element.total * element.menu.price;
      countTotal = countTotal + total;
    }
    return countTotal;
  }

  static countGrandTotalWithTax(subTotal: number): number {
    if (subTotal <= 0) {
      return 0;
    }

    // PPN 11% = 0,11
    const tax = subTotal * 0.11;

    // subTotal with tax
    return subTotal + tax;
  }

}
