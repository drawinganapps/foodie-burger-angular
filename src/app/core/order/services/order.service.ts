import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {Order} from "../../models/order";
import {Menu} from "../../models/menu";

@Injectable()
export class OrderService {
  private _order$: BehaviorSubject<Order> = new BehaviorSubject<Order>({orderedMenu: []});

  constructor() { }

  addMenuToOrder(menu: Menu, quantity: number): void {
    let order = this._order$.getValue();
    const currentMenuIndex = order.orderedMenu.findIndex(currentMenu => currentMenu.menu.id === menu.id);

    if(currentMenuIndex > -1) {
      order.orderedMenu[currentMenuIndex].total += quantity;
    } else {
      order.orderedMenu.push(
        {
          menu: menu,
          total: quantity
        }
      );
    }
    this._order$.next(order);
  }

  increaseOrderQuantity(menu: Menu): void {
    let order = this._order$.getValue();
    const currentMenuIndex = order.orderedMenu.findIndex(currentMenu => currentMenu.menu.id === menu.id);

    if(currentMenuIndex > -1) {
      order.orderedMenu[currentMenuIndex].total += 1;
      this._order$.next(order);
    }
  }

  decreaseOrderQuantity(menu: Menu): void {
    let order = this._order$.getValue();
    const currentMenuIndex = order.orderedMenu.findIndex(currentMenu => currentMenu.menu.id === menu.id);

    if(currentMenuIndex > -1) {
      if (order.orderedMenu[currentMenuIndex].total > 1) {
        order.orderedMenu[currentMenuIndex].total -= 1;
      } else {
        order.orderedMenu.splice(currentMenuIndex, 1);
      }

      this._order$.next(order);
    }
  }

  removeMenuFromOrder(menu: Menu): void {
    let order = this._order$.getValue();
    const currentMenuIndex = order.orderedMenu.findIndex(currentMenu => currentMenu.menu.id === menu.id);

    if(currentMenuIndex > -1) {
      order.orderedMenu.splice(currentMenuIndex, 1);
      this._order$.next(order);
    }
  }

  orderedMenu$(): Observable<Order> {
    return this._order$.asObservable();
  }
}
