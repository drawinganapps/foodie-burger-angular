import { Injectable } from '@angular/core';
import {Menu} from "../../models/menu";
import {Observable, of} from "rxjs";
import {DummyData} from "../../../shared/dummy-data";

@Injectable()
export class MenuService {

  constructor() { }

  getAllMenu(): Observable<Menu[]> {
    return of(DummyData.menuList);
  }
}
