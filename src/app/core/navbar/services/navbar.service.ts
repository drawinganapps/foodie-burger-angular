import { Injectable } from '@angular/core';
import {NavigationBar, NestedNavigationBar} from "../../models/navigation-bar";
import {DummyData} from "../../../shared/dummy-data";

@Injectable()
export class NavbarService {

  constructor() { }

  navbarItems(): NestedNavigationBar[] {
    return DummyData.navigationBar;
  }
}
